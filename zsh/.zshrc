############################################## -*- conf -*-
# Torsteiun's ZSH conf
##############################################

## Fuzzy search
export FZF_DEFAULT_COMMAND='ag --hidden --ignore .git -g ""'

## ZSH function path
fpath=( "$HOME/.zsh/zsh-functions" $fpath )

## ZSH completion of various commands
source ~/.zsh/zsh-completions/zsh-completions.plugin.zsh
autoload -U compinit && compinit

## Colours
export TERM=xterm-256color

## Bash-like navigation
autoload -U select-word-style
select-word-style bash

## Aliases
source $HOME/.bashrc.aliases

## Editor
# See https://emacsredux.com/blog/2020/07/16/running-emacs-with-systemd/
# export ALTERNATE_EDITOR=''
export EDITOR=vim
# zsh will set vim bindings if EDITOR is set to vim, so we'll need to
# explicitly tell it to use Emacs bindings:
bindkey -e

## PATH
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64
export PATH=${HOME}/bin:\
${HOME}/.local/bin:\
${JAVA_HOME}/bin:\
${HOME}/src/my-little-friends/bash:\
${HOME}/src/hobbiton/dev-commands:\
${HOME}/src/cannon/bin:\
${HOME}/src/dr/bin:\
/opt/apache-maven-3.6.1/bin:\
/snap/bin:\
${PATH}

# Enable fuzzy search
source /usr/share/doc/fzf/examples/key-bindings.zsh

# History
setopt INC_APPEND_HISTORY
setopt SHARE_HISTORY
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_IGNORE_DUPS
HISTSIZE=999999999
SAVEHIST=$HISTSIZE
HISTFILE=$HOME/.zsh_history

# Flatpak
export XDG_DATA_DIRS=/var/lib/flatpak/exports/share:\
$HOME/.local/share/flatpak/exports/share:\
$XDG_DATA_DIRS

# Man pages
export MANPAGER="sh -c 'col -bx | batcat -l man -p'"

## Exit shell without having to disown the background processes. This
## makes the ZSH behavour like that of BASH.
setopt NO_HUP
setopt NO_CHECK_JOBS

## Don't fail if using regexp in commands like "find -name *.zip" and
## the regexp doesn't match (in the current directory). Behaviour thus
## becomes the same as in bash.
unsetopt nomatch

## Auto suggestions
source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh

## In-shell syntax highlighting
source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

## Auto completion for OpenShift
# Turned off for now, it messes up the foreground colour for some
# reason, often setting it black (!)
# source ~/.zsh/oc-completion.zsh

## My very own prompt
export PROMPT='%1~ %(?.%F{green}$.%F{red}$)%f '

## Show Git branch to the right
autoload -Uz vcs_info
precmd_vcs_info() { vcs_info }
precmd_title() { print -Pn "\e]0;%n@%m: %~\a" }
precmd_functions+=(  precmd_title precmd_vcs_info )
setopt prompt_subst
RPROMPT=\$vcs_info_msg_0_
zstyle ':vcs_info:git:*' formats '%F{240}%b%f' # '%b'

## Set window title to the current directory
precmd () {
  print -Pn "\e]0;%~\a"
}

## Support machine local overrides and private additions
if [ -r "${HOME}"/.zshrc.private ]; then
  source $HOME/.zshrc.private
fi

# Support machine dependent settings. This is useful when mounting the
# home directory into containers and VMs.
if [ -r "${HOME}"/.zshrc.$(hostname) ]; then
  source $HOME/.zshrc.$(hostname)
fi
