#! /usr/bin/env bash

## Command to create an agenda view from org mode suitable for emails
## and Status Hero.

##          author: torstein@escenic.com
set -o errexit
set -o nounset
set -o pipefail
shopt -s nullglob

create_report() {
  format=${1-"markdown"}
  days_back=${2-"7"}
  now=
  start_date=
  org_agenda=

  now=$(date +%s)
  start_date=$(date +%Y-%m-%d --date @$((now - (60 * 60 * 24 * days_back))))
  local tmp=
  tmp=$(mktemp)
  cat > "${tmp}"  <<EOF
(setq org-todo-keywords
  '((sequence "TODO(t)" "STARTED(s)" "WAITING(w)" "PR(p)" "|"
              "MERGED(m)" "DONE(d)" "CANCELLED(c)" "DELEGATED(g)"))
)

(org-batch-agenda "a"
          org-agenda-start-day "${start_date}"
          org-agenda-span $(( days_back + 1 ))
          org-agenda-include-diary t
          org-agenda-sorting-strategy '(todo-state-up)
          org-agenda-files '("~/doc/scribbles/$(date +%Y)"))
EOF
  org_agenda=$(
    /usr/bin/emacs -batch -l "${tmp}" 2>/dev/null |
      grep -v 'life:' |
      grep -v 'leaves:' |
      grep -v 'quotes:' |
      grep -v 'yt:')

  rm -rf "${tmp}"

  result=
  result=$(
    echo "${org_agenda}" |
      sed '/:noreport:/d' |
      sed -r 's#([a-z]+): .* Sched.*: (.*)#\2#' |
      sed -r 's#^[ ]*DONE# ✔#g' |
      sed -r 's#^[ ]*STARTED# ▶#g' |
      sed -r 's#^[ ]*TODO##g' |
      sed -r 's#^[ ]*WAITING(.*)# ⌛ \1 (waiting) #' |
      sed -r 's#^[ ]*PR(.*)# ⌛\1 (in PR review)#g' |
      sed -r 's#^[ ]*MERGED(.*)# ✔\1 (merged)#' |
      sed -r 's#talk(ed)* with #💬 with #i' |
      sed -r 's#[ ]*[(][0-9]+/[0-9]+[)]: ##' |  # repeating events like 4/99
      sed -r 's#^[ ]*gcal:[ ]* .*[0-9]?[0-9]:[0-9][0-9] (.*)# Meeting: \1#' |
      sed -r 's#^[ ]*gcal:# Meeting:#' |        # events without start/end date
      sed -r 's#^[ ]*outlook:[ ]* .*[0-9]?[0-9]:[0-9][0-9] (.*)# Meeting: \1#' |
      sed -r 's#^[ ]*outlook:# Meeting:#' |        # events without start/end date
      sed -r 's#([A-Z]+[-][0-9]+)#\1#g' |
      sed -r '/[a-z]/!d' |
      sed -r '/football:/Id' |
      sed -r '/.... now - - -/d' |
      sed -r 's#(.*Meeting: .*) -.*#\1#' |
      sed -r 's#([A-Z]+[-][0-9]+) (.*)# Jira:\1 \2#'
        )

  if [[ ${format} == "markdown" ]]; then
    echo "${result}" | \
      sed -r 's#^[ ]+#- #'
  else
    echo "${result}"
  fi
}

main() {
  create_report markdown $1

  local file=/var/www/html/agenda.md.txt

  create_report markdown 1 | sed '1d' > "${file}"
  #  add bom
  sed -i '1s/^/\xef\xbb\xbf/' "${file}"
  git commit "${file}" -m "Updated agenda $(fortune | head -n 1)" &> /dev/null
}

main "$*"
