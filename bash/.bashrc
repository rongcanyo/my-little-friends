# Torstein's .bashrc

##################################################################
## window
##################################################################
shopt -s checkwinsize
PROMPT_COMMAND='echo -ne "\033]0;${USER-${USERNAME}}@${HOSTNAME}: ${PWD/$HOME/~}\007"'
export TERM=xterm-256color

##################################################################
# prompt
##################################################################
function get_vc_status() {
  local old_exit_code=$?
  local status=$(git status 2>/dev/null | head -1 | awk '{print $NF}')
  if [[ "$status" == "on" ]]; then
    echo "tag/$(git describe --always --tag) "
  elif [ -n "$status" ]; then
    echo "${status} "
  fi
  return $old_exit_code
}

function get_short_dir() {
  local dir=$(pwd)
  dir=${dir#/home/${USER}}
  if [ -z "${dir}" ]; then
      dir="~"
  fi
  ## TODO
  echo $dir
}

PS1="\[\033[0;35m\]\$(get_vc_status)\[\033[0;39m\]\W \$(if [ \$? -ne 0 ]; then echo '↓ '; fi)\[\033[0;32m\]$\[\033[0;39m\] "

# improved bash -x (must be exported)
export PS4='# ${BASH_SOURCE}:${LINENO}: ${FUNCNAME[0]}() - [${SHLVL},${BASH_SUBSHELL},$?] '

##################################################################
# history
##################################################################
shopt -s histappend
HISTCONTROL=ignoredups
HISTSIZE=1000000
HISTFILESIZE=1000000
HISTTIMEFORMAT="%F %H:%M:%S "
PROMPT_COMMAND="${PROMPT_COMMAND:+$PROMPT_COMMAND ;}"\
'echo "$(history 1)" >> $HOME/.bash_eternal_history'

##################################################################
# aliases
##################################################################
source $HOME/.bashrc.aliases

# If we're on OpenBSD, redefine some aliases
if [[ $(uname -s) == OpenBSD ]]; then
  alias ls='gls -ltrh --color'
  alias grep='grep --text --ignore-case'
fi
##################################################################
## Command for switchong to a simple prompt I always use this in Emacs
## shells, so I set the EDITOR variable to emacsclient as well.
##################################################################
function uprompt() {
  export EDITOR=emacsclient
  PROMPT_COMMAND='echo "$(history 1)" >> $HOME/.bash_eternal_history'
  export PS1='\$ '
}

##################################################################
# bash completion
##################################################################
# music → cd music
shopt -s autocd
# ls src/**/pom.xml
shopt -s globstar

export FIGNORE=.svn
l="
  $HOME/src/my-little-friends/bash_completion.d/tkj
  $HOME/src/ece-scripts/etc/bash_completion.d/ece
  $HOME/src/moria/voss/etc/bash_completion.d/p4
  $HOME/src/my-little-friends/bash_completion.d/mvn
  $HOME/src/moria/src/ecews/bin/ecews.bash_completion

  /etc/bash_completion
  /usr/share/bash-completion/bash_completion
  /etc/bash_completion.d/subversion
  /usr/share/bash-completion/git
"
for el in $l; do
  if [ -r $el ]; then
    source $el
  fi
done

##################################################################
# java
##################################################################
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64
export MAVEN_OPTS="-Xmx1024m"

##################################################################
# man pages from non packages
##################################################################
export MANPATH=/usr/local/man/man1:$MANPATH

##################################################################
# path
##################################################################
PATH=\
$HOME/bin:\
$HOME/.cargo/bin:\
$HOME/.local/bin:\
$HOME/src/dr/bin:\
$HOME/src/ece-scripts/usr/bin:\
$HOME/src/hobbiton/dev-commands:\
$HOME/src/moria/voss/usr/bin:\
$HOME/src/my-little-friends/bash:\
$HOME/src/my-little-friends/bash/vcs:\
$HOME/src/my-little-friends/git:\
$HOME/src/moria/src/net:\
$HOME/src/moria/src/java:\
$HOME/src/moria/src/graphics:\
$HOME/src/moria/src/pictures:\
/opt/local/bin:\
$JAVA_HOME/bin:\
$PATH

export CDPATH=:$HOME/src:$HOME/doc:$HOME

##################################################################
# editor
##################################################################
export EDITOR=vim

##################################################################
# language & time zone
##################################################################
# export TZ='Asia/Taipei'
export LANG=en_GB.UTF-8
export LC_ALL=en_GB.UTF-8

##################################################################
# shell check
##################################################################
export SHELLCHECK_OPTS='--shell=bash'

##################################################################
# local overrides & private bash settings
##################################################################
if [ -e ~/.bashrc.private ]; then
  source ~/.bashrc.private
fi
