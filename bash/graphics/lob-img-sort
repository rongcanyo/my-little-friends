#! /usr/bin/env bash

## Command to sort a directory of images by their GPS coordinates. The
## command will sort the pictures into three directories:
## - on location, e.g. work
## - off location, e.g. home
## - no geotag, i.e. pictures with no GPS meta tags
##
## author: torstein

set -o errexit
set -o nounset
set -o pipefail

src_dir=$(pwd)
no_geotag_dir=/tmp/img-with-no-geo-tag
off_location_dir=/tmp/img-home
on_location_dir=/tmp/img-work
on_location_latitude="6/1"
on_location_latitude2="7/1"
on_location_longitude="19/1"

show_help_and_exit() {
  cat <<EOF
Usage: ${BASH_SOURCE[0]} [OPTIONS]

OPTIONS
  -h, --help   Don't panic.
EOF
}

read_user_input() {
  local OPTS=
  OPTS=$(getopt \
           -o vnhs: \
           --long dry-run \
           --long help \
           --long no-geotag-dir: \
           --long off-location-dir: \
           --long on-location-dir: \
           --long on-location-latitude: \
           --long on-location-latitude2: \
           --long on-location-longitude: \
           --long src: \
           --long verbose \
           -n 'parse-options' \
           -- "$@")
  if [ $? != 0 ] ; then
    echo "Failed parsing options." >&2
    exit 1
  fi
  eval set -- "$OPTS"

  while true; do
    case "$1" in
      -h | --help )
        show_help_and_exit;
        break;;
      -s | --src )
        src_dir=$2
        shift 2;;
      --no-geotag-dir )
        no_geotag_dir=$2
        shift 2;;
      --on-location-dir )
        on_location_dir=$2
        shift 2;;
      --on-location-latitude )
        on_location_latitude=$2
        shift 2;;
      --on-location-latitude2 )
        on_location_latitude2=$2
        shift 2;;
      --on-location-longitude )
        on_location_longitude=$2
        shift 2;;
      --off-location-dir )
        off_location_dir=$2
        shift 2;;
      -n | --dry-run )
        export dry_run=1
        shift;;
      -v | --verbose )
        export verbose=1
        shift;;
      -- )
        shift;
        break ;;
      * )
        break ;;
    esac
  done

  rest_of_args=$*
}

_is_verbose() {
  [ ${verbose-0} -eq 1 ]
}

_is_dry_run() {
  [ ${dry_run-0} -eq 1 ]
}

_debug() {
  if _is_verbose; then
    printf "DEBUG %s\\n" "${*}"
  fi
}


## $1 :: file
_is_on_location() {
  local _latitude=$1
  local _longitude=$2

  IFS=, read -r ignore _latitude_part ignore2 <<< "${_latitude// /}"
  IFS=, read -r ignore _longitude_part ignore2 <<< "${_longitude// /}"


  _debug "f=${f}" \
         "latitude=${_latitude_part}" \
         "longitude=${_longitude_part}"

  set -o errexit
  [[ "${_longitude_part}" == "${on_location_longitude}" &&
       "${_latitude_part}" == "${on_location_latitude}" ]] ||
    [[ "${_longitude_part}" == "${on_location_longitude}" &&
         "${_latitude_part}" == "${on_location_latitude2}" ]]
}

_mv() {
  local f=$1
  local dir=$2

  if _is_dry_run; then
    printf "\\tmv %s %s\\n" "${f}" "${dir}"
  else
    mv -v -i "${f}" "${dir}" || true
  fi
}

_sort_img() {
  find "${src_dir}" -iname "*.jpg" -o -iname "*.png" -o -iname "*.jpeg" |
    while read -r f; do
      local _latitude_longitude=
      _latitude_longitude=$(
        identify -verbose "${f}" |
          sed -n -r 's#exif:GPS(Longitude|Latitude):#\1#p')

      if [ -z "${_latitude_longitude}" ]; then
        _mv "${f}" "${no_geotag_dir}"
        continue
      fi

      local _latitude=$(
        printf "%s\\n" "${_latitude_longitude}" |
          sed -n -r 's#.*Latitude ##p')
      local _longitude=$(
        printf "%s\\n" "${_latitude_longitude}" |
          sed -n -r 's#.*Longitude ##p')


      if _is_on_location "${_latitude}" "${_longitude}"; then
        _mv "${f}" "${on_location_dir}"
      else
        _mv "${f}" "${off_location_dir}"
      fi
    done
}

_ensure_sanity() {
  mkdir -p "${on_location_dir}"
  mkdir -p "${off_location_dir}"
  mkdir -p "${no_geotag_dir}"
}


main() {
  read_user_input "$@"
  _ensure_sanity
  _sort_img
}

main "$@"
