#! /usr/bin/env bash

# by torstein@escenic.com

set -o errexit
set -o nounset
set -o pipefail

show_help_and_exit() {
  cat <<EOF
Usage: ${BASH_SOURCE[0]} [OPTIONS]

OPTIONS
  -h, --help   Don't panic.
EOF
}

read_user_input() {
  local OPTS=
  OPTS=$(getopt \
           -o h \
           --long help \
           --long name: \
           --long engine-dir: \
           -n 'parse-options' \
           -- "$@")
  if [ $? != 0 ] ; then
    echo "Failed parsing options." >&2
    exit 1
  fi
  eval set -- "$OPTS"

  while true; do
    case "$1" in
      -h | --help )
        show_help_and_exit;
        break;;
      --name )
        local db_name=$2
        export db_schema=${db_name}db
        export db_user=${db_name}user
        export db_password=${db_name}password

        shift 2;;
      --engine-dir )
        export engine_dir=$2
        shift 2;;
      -- )
        shift;
        break ;;
      * )
        break ;;
    esac
  done

  rest_of_args=$*
}

_create_db() {
  mysql <<EOF
create database $db_schema character set utf8 collate utf8_general_ci;
grant all on $db_schema.* to $db_user@'%' identified by '$db_password';
grant all on $db_schema.* to $db_user@'localhost' identified by '$db_password';
EOF
}

_run_ece_sql_scripts() {
  find "${engine_dir-/usr/share/escenic}" -name mysql -type d |
    head -n 1 |
    while read -r mysql_dir; do
      for el in tables constraints indexes constants; do
        local file=${mysql_dir}/${el}.sql
        if [ -r "${file}" ]; then
          mysql -u "${db_user}" -p"${db_password}" "${db_schema}" < "${file}"
        else
          printf "%s doesn't contain %s\\n" "${mysql_dir}" "${el}"
          exit 1
        fi
      done
    done
}


main() {
  read_user_input "$@"
  _create_db
  _run_ece_sql_scripts
}

main "$@"
