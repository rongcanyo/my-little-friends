#! /usr/bin/env bash

# by torstein.k.johansen@gmail.com

log_file=$HOME/.$(basename $0).log
LC_ALL=C
conf_file=$HOME/.$(basename $0).conf

src_list="
"
java_src_list="
"
java_build_src_list="
"
eclimd_bin=/opt/eclipse/eclimd
scm_user_list="h73 tkj torstein"

function read_user_conf_if_exists() {
  if [ -r $conf_file ]; then
    source $conf_file
  fi
}

function do_create-emacs-tarball() {
  local file=$HOME/tmp/tkj-emacs-$(date --iso).tar.gz
  local tmp_dir=
  tmp_dir=$(mktemp -d)

  # emacs conf + workspaces + jdk
  cp -r ~/src/my-little-friends/emacs/.emacs "${tmp_dir}"
  cp -r ~/src/my-little-friends/emacs/.emacs.d "${tmp_dir}"

  # lombok
  lombok_jar_file=.m2/repository/org/projectlombok/lombok/1.18.6/lombok-1.18.6.jar
  lombok_jar_target_dir="${tmp_dir}/$(dirname "${lombok_jar_file}")"
  mkdir -p "${lombok_jar_target_dir}"
  cp "${HOME}/${lombok_jar_file}" "${lombok_jar_target_dir}"

  tar -C "${tmp_dir}" -czf  "${file}" ./
  rm -rf "${tmp_dir}"
}

function do_talk-start() {
  xrdb -merge ~/src/my-little-friends/x/x-resources-presentation
  urxvt +sb -cr red -bc -sl 10000 -fn 'xft:Source Code Pro' -bg white -fg black &
  if [ -n "$1" ]; then
    do_setup-screens-presentation
  fi
}

function do_talk-stop() {
  xrdb -remove
  xrdb -merge ~/src/my-little-friends/x/x-resources
  xrdb -merge "$HOME"/src/my-little-friends/x/x-tango-theme
}

## $1 :: project
function do_schedule-hudson-build() {
  print "Scheduled a new build of" $1 "..."
  curl -s "http://hudson.dev.escenic.com/job/${1}/build?delay=0sec"
  print "Check back here for progress: " \
        http://hudson.dev.escenic.com/job/${1}/lastBuild
}

function do_go-to-meeting() {
  do_turn-off-external-screens
  su -c wireless
  vpn
}

function do_restart-music() {
  killall -9 pulseaudio &> /dev/null
  pulseaudio --start --log-target=syslog
  killall -9 spotify &> /dev/null
  spotify &
  do_music-toggle
}

function do_agenda() {
  org-agenda.sh | while read -r line; do
    if [[ "${line}" == *Monday* ||
            "${line}" == *Tuesday* ||
            "${line}" == *Wednesday* ||
            "${line}" == *Thursday* ||
            "${line}" == *Friday* ||
            "${line}" == *Staturday* ||
            "${line}" == *Sunday* ]]; then
      tput setaf 1
      printf "%s\n" "${line}"
      tput sgr 0
    else
      echo "${line}"
    fi
  done
}

## Method useful for developers and system administrators. It checks
## the passed URL for common things interesting, producing a short
## report, as well as listing all the response headers.
function do_test-url()
{
  local headers="$(
        wget --quiet \
            --server-response \
            --header='Accept-Encoding: gzip,deflate' \
            $1 \
            2>&1 >/dev/null
    )"

  if [ $(echo "$headers" | grep "Set-Cookie" | wc -l) -gt 0 ]; then
    print "Cache-ability: not ok"
  else
    print "Cache-ability: ok"
  fi

  if [ $(echo "$headers" | grep "Content-Encoding: gzip" | wc -l) -gt 0 ]; then
    print "Compression:   ok"
  else
    print "Compression:   not ok"
  fi

  print "Response headers:"
  echo "$headers"
}

function do_help()
{
  print "$(basename $0) supports these commands:"
  declare | grep ^"do_" | cut -d' ' -f1 | cut -d'_' -f2 | sort | \
    while read f; do
      echo "  *" $f
    done
}

function do_set-up-vpn() {
  if [ -x /usr/sbin/vpnc ]; then
    print "Setting up VPN ..."
    sudo vpnc default
  fi
}

function do_generate-eclipse-files-and-restart-eclimd() {
  local dir=
  for dir in $eclipse_workspace_src_list; do
    print "Deleting old Eclipse files in ${dir} ..."
    find "${dir}" \
         -name bin -o \
         -name .classpath -o \
         -name maven-eclipse.xml -o \
         -name .externalToolBuilders -o \
         -name .project \
         -print0 | \
      xargs -0 rm -rf

    print "Re-generating Eclipse project files in ${dir} ..."
    run mvn -q \
        -f "${dir}"/pom.xml \
        -DdownloadSources \
        -DdownloadJavadocs \
        -Dmaven.test.skip=true \
        clean \
        install \
        eclipse:clean \
        eclipse:eclipse
  done

  do_restart-eclimd ${1-engine}
}

function do_us() {
  setxkbmap us -option ctrl:nocaps
  [ -r ~/.xmodmaprc ] && xmodmap ~/.xmodmaprc
}

function do_update-eclipse-files-and-restart-eclimd() {
  local dir=
  for dir in $eclipse_workspace_src_list; do
    print "Updating Eclipse project files in ${dir} ..."
    run mvn -q \
        -f "${dir}"/pom.xml \
        -DdownloadSources \
        -DdownloadJavadocs \
        -Dmaven.test.skip=true \
        clean \
        install \
        eclipse:eclipse
  done

  do_restart-eclimd engine
}

function do_lock-screen() {
  local xscreensaver=$1

  do_music-stop
  killall gnome-screensaver 2>/dev/null

  if [ -n "${xscreensaver}" ]; then
    xscreensaver 2>/dev/null &
    sleep 1
    xscreensaver-command -lock
  elif [ -x /usr/bin/i3lock ]; then
    i3lock \
      --image ~/pictures/wallpapers/current.png \
      --tiling \
      --color 000000
  elif [ -x /usr/local/bin/i3lock ]; then
    i3lock --blur 5
  fi

}

function do_sync-scribbles() {
  local file=$HOME/doc/scribbles/bin/sync
  if [ -x "${file}" ]; then
    "${file}" > /dev/null
  fi
}

function do_remove-various-junk() {
  find "${HOME}" \
       -maxdepth 5 \
       -name "*.jvm1" -o \
       -name "replay_*.log" -o \
       -name "hs_err_*" |
    while read -r f; do
      rm -r "${f}"
    done
}

function do_sync-google-tasks() {
  local cmd=
  cmd=~/doc/scribbles/$(date +%Y)/sync-google-tasks
  if [ -x "${cmd}" ]; then
    ${cmd}
  fi
}

function do_daily()
{
  local today_dir=
  local doc_dir=
  doc_dir=$HOME/doc/$(date +%Y)
  today_dir="${doc_dir}/$(date --iso)"

  print "Getting new email ..."
  get-new-email &

  print "Removing empty directories in ${doc_dir} ..."
  find "${doc_dir}" -type d -empty -delete &

  print "Creating daily doc dir ${doc_dir} ..."
  mkdir -p "${today_dir}" &> /dev/null

  local today="${HOME}/doc/today"
  print "Pointing ${today} to ${today_dir} ..."
  if [ -h "${today}" ]; then
    rm "${today}"
  fi
  if [ ! -e "${today}" ]; then
    (cd "${HOME}/doc" && ln -s "${today_dir}" "${today}")
  fi

  do_remove-various-junk
  do_sync-scribbles
  # do_sync-google-tasks

  if [ $(ssh-add -l | grep -v "no identities" | wc -l) -lt 1 ]; then
    print "Adding your SSH key"
    ssh-add
  fi

  print "Setting wallpaper..."
  do_set-wallpaper

  print "Stopping all Java processes"
  killall java || true

  # do_build_and_deploy_ece_to_pull_down_todays_snapshots
  do_backup

  print "Happy hacking :-)"
}

do_restart_virtual-machines() {
  print "Restarting virtual machine(s) ..."
  local vms="iam"
  vms=""
  for vm in ${vms}; do
    vboxmanage controlvm "${vm}" poweroff &> /dev/null || true
    run vboxmanage startvm "${vm}" --type headless
  done
}

do_build_and_deploy_ece_to_pull_down_todays_snapshots() {
  print "Building ECE to pull down today's SNAPSHOT dependencies ..."
  pom=~/src/content-engine/pom.xml
  sed -i  '/<module>demo<\/module>/d' "${pom}"
  sed -i  '/<module>documentation<\/module>/d' "${pom}"
  sed -i  '/<module>studio-webstart<\/module>/d' "${pom}"
  sed -i  '/<module>engine-dist<\/module>/d' "${pom}"
  mvn \
    --fail-at-end \
    -Dmaven.test.skip=true \
    clean \
    install \
    -f ~/src/content-engine/pom.xml \
    &> /dev/null
  (cd ~/src/content-engine && git checkout pom.xml)

  print "Now deploying ECE ..."
  tcc &>/dev/null
}


function do_backup() {
  if [ -x ~/backup/sync ]; then
    ~/backup/sync
  fi
}

function do_restart-eclimd() {
  local eclimd_pid=$(ps aux|grep $eclimd_bin | grep -v grep | awk '{print $2;}  ')
  if [ -n "$eclimd_pid" ]; then
    print "Stopping eclimd ..."
    kill -9 $eclimd_pid
  fi

  do_start-eclimd "$@"
}

function do_start-eclimd() {
  print "Starting eclimd ..."

  local workspace=@user.home/src/workspace-eclim
  if [ -n "$1" ]; then
    workspace=@user.home/src/workspace-${1}
  fi
  xterm -bg black -fg green -e $eclimd_bin \
        -Dosgi.instance.area.default=${workspace} &
}

function do_set-mvn-version() {
  if [ -e pom.xml ]; then
    mvn -q versions:set -DnewVersion=${1}
  fi
  if [ -e .git ]; then
    git commit -a -m "- set version to ${1}"
  fi
}

function do_compile() {
  local module_list="
    docengine
  "

  local mvn_opts="
    -Dfindbugs.skip=true
    -Dpmd.skip=true
    -Dcheckstyle.skip=true
    -DskipTests
  "

  for src in $java_build_src_list; do
    for el in $module_list; do
      if [ ! -e ${src}/${el}/pom.xml ]; then
        continue
      fi

      print "Compiling $src/$el ..."
      time mvn -o -q -f ${src}/${el}/pom.xml $mvn_opts package
    done
  done
}

function do_build-sources() {
  for el in $java_build_src_list; do
    if [ -e $el/pom.xml ]; then
      print "Building $el ..."
      run cd $el
      run mvn clean install eclipse:eclipse \
          -Dpmd.skip=true \
          -Dfindbugs.skip=true \
          -Dcheckstyle.skip=true \
          -DskipTests
    fi
  done
}

function do_update-tags()
{
  for el in $src_list; do
    if [ ! -d $el ]; then
      continue
    fi

    print "Updating etags for $el ..."
    (
      run cd $el
      find . -type f | \
        egrep -v ".svn|.git|.hg|~|.swp|.class|target|TAGS" | \
        etags -
    )
  done

  for el in ${eclipse_workspace_src_list}; do
    if [ ! -d "$el" ]; then
      continue
    fi

    print "Updating gtags for $el ..."
    (
      run cd $el
      find . -type f -name "*.java" | gtags -f -
    )
  done
}

function do_doc() {
  print "Documentation for method ${1}:"
  grep -B 5 "^function do_${1}" $(which $0) | grep ^"##" | cut -d' ' -f2- | \
    while read line; do
      echo '   '"$line"
    done
}

function do_status() {
  for el in $src_list; do
    if [ ! -d "$el" ]; then
      continue
    fi
    (
      cd $el
      if [ -d $el/.git ]; then
        local status=$(git status | egrep "(modified:|added:|new file:|ahead of)")
        if [ $(echo "$status" | wc -c) -gt 2 ]; then
          echo "${el} $(git status | head -1 | sed 's#On branch ##')"
          echo "$status"
        fi
      fi
    )
  done

  local status=$(p4 opened 2>/dev/null)
  if [ -n "${status}" ]; then
    echo "Checked out from p4:"
    red "$(echo "$status" | cut -d'#' -f1)"
  fi
}

## Updates the sources, including p4, git, forks, hg++
## Arguments: takes no arguments.
function do_update-sources()
{
  for el in $src_list; do
    if [ ! -d $el ]; then
      continue
    fi

    print "Updating $(echo $el | sed "s#${HOME}/##g") ..."
    (
      cd $el
      if [[ -d $el/.git && $(grep svn-remote $el/.git/config | wc -l) -gt 0 ]]; then
        git checkout --quiet master && \
          git svn rebase
      elif [ -d $el/.git ]; then
        if [[ $(git branch | grep develop | wc -l) -gt 0 ]]; then
          git checkout --quiet develop
        fi
        if [[ $(cd $el && git branch --all | grep remotes/p4/master | wc -l) -gt 0 ]]; then
          git p4 rebase | \
            egrep -v "Current branch|No changes to import|Rebasing the current branch onto|Performing incremental import|Depot paths:|Import destination: refs/remotes/p4/master|Importing revision"
        else
          git pull --quiet --rebase
        fi
      elif [ -d $el/.svn ]; then
        svn up
      elif [ -d $el/CVS ]; then
        cvs up -d
      else
        p4 sync "${el}"/...
      fi
    )
  done

  for el in $HOME/src/forks/*; do
    if [ ! -d $el ]; then
      continue
    fi
    print "----------------------------------------------"
    print "Updating fork $(basename $el) ..."
    print "----------------------------------------------"
    (
      cd $el
      git fetch upstream
      git merge upstream/master
      git push
    )
  done
}

function get_connected_screen_list() {
  xrandr -q | sed -r -n 's#(.*) connected .*#\1#p'
}

function get_full_screen_list() {
  xrandr -q | sed -r -n 's#(.*) (dis)?connected.*#\1#p'
}

function get_first_external_screen() {
  get_connected_screen_list |
    sed '1d;' |
    head -n 1
}

function get_primary_screen() {
  get_connected_screen_list |
    head -n 1
}

function do_setup-screens-presentation() {
  xrandr \
    --output $(get_first_external_screen) \
    --auto \
    --same-as \
    $(get_primary_screen)
  do_set-wallpaper
}

function do_turn-off-external-screens() {
  local screen_list=$(get_full_screen_list)
  local primary_screen=$(echo "${screen_list}" | head -1)
  for screen in ${screen_list}; do
    if [[ "${screen}" == "${primary_screen}" ]]; then
      continue
    else
      xrandr --output ${screen} --off
    fi
  done

  do_set-wallpaper &>/dev/null &
  killall xmessage &> /dev/null || true
}

function do_setup-screens-above-laptop() {
  xrandr \
    --output $(get_first_external_screen) \
    --above $(get_primary_screen) \
    --auto
  sleep 3
  killall xmessage || true
  do_set-wallpaper
}

## Sets up screens like this:
## 💻 → 💻
## ↑
## L
function do_setup-screens-laptop-is-king() {
  xrandr --output HDMI-1 --above eDP-1 --auto
  xrandr --output HDMI-2 --right-of HDMI-1  --auto
  do_set-wallpaper
}

function do_setup-screens() {
  local portrait=${1-0}
  local screen_list=$(get_connected_screen_list)
  local primary_screen=$(get_primary_screen)
  for screen in ${screen_list}; do
    if [[ -z "${previous_screen}" ]]; then
      print "Setting up screen ${screen} ..."
      xrandr --output ${screen} --auto
    else
      print "Setting up screen ${screen} right of ${previous_screen} ..."
      if [ ${portrait} -eq 0 ]; then
        xrandr --output ${screen} --right-of ${previous_screen} --auto
      else
        xrandr --output ${screen} --right-of ${previous_screen} --auto  --rotate left
      fi
    fi
    previous_screen=${screen}
  done

  do_set-wallpaper
}

function do_change-clipboard() {
  if [ $(ps aux| grep clipit | grep -v grep | wc -l) -gt 0 ]; then
    killall -9 clipit 2>/dev/null
    xclipboard -rv -fn 6x12 &
  else
    killall -9 xclipboard 2>/dev/null
    clipit &> /dev/null  &
  fi
}

function do_update-ensime() {
  local pom=~/src/content-engine/pom.xml
  cat > "${pom}" <<'EOF'
<?xml version="1.0" encoding="UTF-8"?>
<project
  xmlns="http://maven.apache.org/POM/4.0.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
                      http://maven.apache.org/maven-v4_0_0.xsd">

  <modelVersion>4.0.0</modelVersion>
  <groupId>com.escenic.core</groupId>
  <artifactId>content-engine-root</artifactId>
  <version>develop-SNAPSHOT</version>
  <packaging>pom</packaging>
  <name>Escenic :: Release POM</name>
  <description>
    This is a POM file that aggregates all Escenic Content Engine
    &amp; Content Studio related products.
  </description>

  <properties>
    <snapshotUrl>
      http://repo.dev.escenic.com/content/repositories/escenic-snapshots
    </snapshotUrl>
  </properties>

  <modules>
    <module>core</module>
    <module>common</module>
    <module>model-core</module>
    <module>engine</module>
    <module>client</module>
    <module>indexer</module>
    <module>studio</module>
    <module>engine-dist</module>
  </modules>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>${project.groupId}</groupId>
        <artifactId>core</artifactId>
        <version>${project.version}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>

  <distributionManagement>
    <repository>
      <id>central</id>
      <url>http://repo.dev.escenic.com/content/repositories/escenic</url>
    </repository>
    <snapshotRepository>
      <id>central</id>
      <url>${snapshotUrl}</url>
      <uniqueVersion>false</uniqueVersion>
    </snapshotRepository>
  </distributionManagement>

  <build>
    <plugins>
      <plugin>
        <groupId>org.ensime.maven.plugins</groupId>
        <artifactId>ensime-maven</artifactId>
        <version>1.1.0</version>
      </plugin>
    </plugins>
  </build>

</project>
EOF

  (
    cd "$(dirname "${pom}")" || exit 1
    mvn ensime:generate
    git revert "$(basename "${pom}")"
  )
}


function do_delete-cache-files()
{
  local dir_list="
      $HOME/.opera/cache
      $HOME/.mozilla/firefox/1tznm2xo.default/Cache
      $HOME/.java/deployment/cache/6.0
      $HOME/.cache/google-chrome
    "

  for el in $dir_list; do
    if [ -d $el ]; then
      rm -r $el
    fi
  done
}

function do_dates()
{
  cat <<EOF
Taipei:    $(TZ='Asia/Taipei' date)
Dhaka:     $(TZ='Asia/Dhaka' date)
Oslo:      $(TZ='Europe/Oslo' date)
Atlanta:   $(TZ=US/Eastern date)
Argentina: $(TZ='America/Argentina/Buenos_Aires' date)
EOF
}

## $2 :: week number
function do_hours-this-week() {
  do_hours &> /dev/null
  local week_number=${1-$(date +%V)}

  local files_this_week=${HOME}/doc/scribbles/$(date +%Y)/hours/"${week_number}"/*.md
  cat ${files_this_week}

  local seconds_left=
  seconds_left=$(
    cat ${files_this_week} |
      awk '{print $11}' | paste -sd+ | bc)
  local hours=$(( seconds_left / ( 60 * 60 ) ))
  local seconds_left=$(( seconds_left - ( $hours * 60 * 60 ) ))
  local minutes=$(( seconds_left / 60 ))
  local seconds=$(( seconds_left - $minutes * 60 ))

  printf "%s hours %s minutes %s seconds \n"\
         "${hours}" "${minutes}" "${seconds}"
}

function do_hours() {
  if [[ $1 ]]; then
    local the_date=$1
  else
    the_date=$(date --iso)
  fi

  local hours=
  hours=$(_hours "$@")

  local dir=
  dir=$HOME/doc/scribbles/$(date +%Y)/hours/$(date +%V)
  mkdir -p "${dir}"
  local file=
  file=${dir}/${the_date}.md
  printf "%s\n" "${hours}" | tee "${file}"
}

function _hours()
{
  if [[ $1 ]]; then
    local the_date=$1
  else
    the_date=$(date --iso)
  fi

  local file=$HOME/.bash_eternal_history

  # Register now
  echo -n '42 ' >> "${file}"
  date '+%Y-%m-%d %H:%M:%S' >> "${file}"

  local entries=$(
    cat "${file}" | \
      awk '{print $2 " " $3}' | \
      grep ${the_date} | \
      sort -n)

  local started_working_string=$(echo "${entries}" | head -1)
  local ended_working_string=$(echo "${entries}" | tail -1)
  local seconds_worked=$((
                          $(date +%s -d "$ended_working_string") -
                          $(date +%s -d "$started_working_string")
                        ))
  local days=$(( seconds_worked / ( 60 * 60 * 24 ) ))
  local seconds_left=$(( seconds_worked - ( $days * 60 * 60 * 24 ) ))
  local hours=$(( seconds_left / ( 60 * 60 ) ))
  local seconds_left=$(( seconds_left - ( $hours * 60 * 60 ) ))
  local minutes=$(( seconds_left / 60 ))
  local seconds_left=$(( seconds_left - $minutes * 60 ))

  # echo "It's" ${hours}h ${minutes}m ${seconds_left}s
  # "since you started working today."$'\n'
  echo -n "|" $(date -d "$ended_working_string" --iso) \
       "|" $(date -d "$started_working_string" +%H:%M) \
       "→" \
       $(date -d "$ended_working_string" +%H:%M) \
       "| "

  if [ $minutes -gt 0 ]; then
    echo -n ${hours}$(echo "scale=2;" $minutes / 60 | bc -l)" hours |"
  else
    echo -n ${hours}".0 hours |"
  fi

  printf " %s | \n" "${seconds_worked}"

  local previous_date
  cat $HOME/.bash_eternal_history | \
    awk '{ print $2; }' | \
    grep $the_date | \
    uniq | \
    sort | \
    while read -r l; do
      now=$(date -d "$l" +%s)
      if [ $previous_date ] ;then
        since_last=$(( $now - $previous_date ))

        if [ $since_last -gt 2500 ]; then
          print "No command line action between" $(date -d @${previous_date} +%H:%M:%S) \
                "and" $(date -d "$l" +%H:%M:%S) \
                $(get_human_time $since_last)
        fi
      fi
      previous_date=$now
    done
}

function eight_days_ago() {
  date --iso --date=@$(( $(date +%s) - $(( 60 * 60 * 24 * 8 )) ))
}

function do_go-home() {
  do_hours
  do_status

  if [ -x ~/doc/scribbles/bin/sync ]; then
    printf "%s\n" "Syncing scribbles ..."
    ~/doc/scribbles/bin/sync
  fi

  do_backup
}

function do_search-im()
{
  logs_dir=$HOME/.purple/logs
  dirs="$logs_dir/jabber/torsteinkrausework@gmail.com/
          $logs_dir/msn/torsteinkrausework@msn.com/
          $logs_dir/irc/torstein@catbert.escenic.com/
          $HOME/.erc/logs
    "
  for dir in $dirs; do
    find $dir -name "*.txt" | xargs grep -n -i --color $1
  done
}

function do_search-mailing-list-archive()
{
  dir=$HOME/mail/mailing-list-archive
  url="http://lists.escenic.com"

  find $dir -name "*.html" | \
    grep -i /[0-9]*.html$ | \
    xargs grep -A 2 -i $1 | \
    sed "s#${dir}#http\:\/#g" | \
    sed "s#html:#html #g"

  local result_html=/tmp/search-results.html
  echo "" > $result_html

  find $dir -name "*.html" | \
    grep -i /[0-9]*.html$ | \
    xargs grep -A 2 -i $1 | while read f; do
    sed "s#${dir}#<p><a href='http\:\/#g" | \
      sed "s#html:#html'>link</a></p>#g" \
          >> ${result_html}
  done
  opera ${result_html}
}

function do_jira()
{
  opera https://jira.vizrt.com/browse/${1}
}

function search_otrs()
{
  url="http://otrs.escenic.com/otrs/index.pl"
  session_header_file=/tmp/`basename $0`-$USER-otrs.cookie

  otrs_user=`cat $HOME/.otrs | grep user | cut -d'=' -f2`
  otrs_password=`cat $HOME/.otrs | grep pass | cut -d'=' -f2`

  if [ -r $session_header_file ]; then
    session_header=`cat $session_header_file`
  else
    session_header=`wget -S -O /dev/null \
            --post-data "Action=Login&RequestedURL=&Lang=en&User=$otrs_user&Password=$otrs_password" \
            $url |& \
            grep Set-Cookie | \
            cut -d':' -f2 | \
            cut -d';' -f1`
    echo $session_header > $session_header_file
  fi

  current_year=`date +%Y`
  last_year=$(( $current_year - 1 ))

  uri_parameters=`wget -O -\
         --post-data "Action=AgentTicketSearch&Subaction=Search&TicketNumber=$1&CustomerID=&CustomerUserLogin=&From=&To=&Cc=&Subject=&Body=&TicketFreeText2=&TicketFreeText4=&TicketFreeText6=&TimeSearchType=&TicketCreateTimePointStart=Last&TicketCreateTimePoint=1&TicketCreateTimePointFormat=day&TicketCreateTimeStartMonth=12&TicketCreateTimeStartDay=5&TicketCreateTimeStartYear=$last_year&TicketCreateTimeStopMonth=1&TicketCreateTimeStopDay=4&TicketCreateTimeStopYear=$this_year&ResultForm=Normal&Profile=" \
         --header "Cookie: ${session_header}" \
         $url \
         2>/dev/null | \
         grep $1 | \
         cut -d'"' -f2 | \
         cut -d'?' -f2`
  opera "$url/?$uri_parameters"
}

function do_music-next() {
  if [ $(ps aux | grep -w xmms | grep -v grep | wc -l) -gt 0 ]; then
    xmms -f
  elif [ $(ps aux | grep -v grep | grep -w -c spotify) -gt 0 ]; then
    dbus-send \
      --print-reply \
      --dest=org.mpris.MediaPlayer2.spotify \
      /org/mpris/MediaPlayer2 \
      org.mpris.MediaPlayer2.Player.Next
  elif [ $(ps aux | grep -w mpd | grep -v grep | wc -l) -gt 0 ]; then
    mpc -p ${mpd_port-6600} next
  fi
}

function do_music-previous() {
  if [ $(ps aux | grep -w xmms | grep -v grep | wc -l) -gt 0 ]; then
    xmms -r
  elif [ $(ps aux | grep -v grep | grep -w -c spotify) -gt 0 ]; then
    dbus-send \
      --print-reply \
      --dest=org.mpris.MediaPlayer2.spotify \
      /org/mpris/MediaPlayer2 \
      org.mpris.MediaPlayer2.Player.Prev
  elif [ $(ps aux | grep -w mpd | grep -v grep | wc -l) -gt 0 ]; then
    mpc -p ${mpd_port-6600} prev
  fi
}

function do_music-toggle() {
  if [ $(ps aux | grep -w xmms | grep -v grep | wc -l) -gt 0 ]; then
    xmms -t
  elif [ $(ps aux | grep -v grep | grep -w -c spotify) -gt 0 ]; then
    dbus-send \
      --print-reply \
      --dest=org.mpris.MediaPlayer2.spotify \
      /org/mpris/MediaPlayer2 \
      org.mpris.MediaPlayer2.Player.PlayPause
  elif [ $(ps aux | grep -w mpd | grep -v grep | wc -l) -gt 0 ]; then
    mpc -p ${mpd_port-6600} toggle
  fi
}

function do_music-stop() {
  if [ $(ps aux | grep -w xmms | grep -v grep | wc -l) -gt 0 ]; then
    xmms -t
  elif [ $(ps aux | grep -v grep | grep -w -c spotify) -gt 0 ]; then
    dbus-send \
      --print-reply \
      --dest=org.mpris.MediaPlayer2.spotify \
      /org/mpris/MediaPlayer2 \
      org.mpris.MediaPlayer2.Player.Stop
  elif [ $(ps aux | grep -w mpd | grep -v grep | wc -l) -gt 0 ]; then
    mpc -p ${mpd_port-6600} stop
  fi
}

function do_music-info() {
  if [ $(ps aux | grep -w xmms | grep -v grep | wc -l) -gt 0 ]; then
    :
  elif [ $(ps aux | grep -v grep | grep -w -c spotify) -gt 0 ]; then
    local info
    info=$(
      qdbus \
        --literal org.mpris.MediaPlayer2.spotify \
        /org/mpris/MediaPlayer2 \
        org.freedesktop.DBus.Properties.Get \
        org.mpris.MediaPlayer2.Player \
        Metadata |
        sed 's#, "#, "\n#g' |
        egrep 'xesam:(title|album|arist)')

    album=$(echo "${info}" | grep -w xesam:album | sed -r 's#xesam:album.*[:] ##')
    artist=$(echo "${info}" | grep -w xesam:albumArtist | sed -r 's#xesam:albumArtist.*[:] ##')
    title=$(echo "${info}" | grep -w xesam:title | sed -r 's#xesam:title.*[:] ##')

    printf "%s ♬ %s ♪ %s\n" "${title}" "${album}" "${artist}" |
      sed 's#[]],##g' |
      sed 's#[{]##g' |
      sed 's#[}]##g' |
      sed 's#["]##g'

  elif [ $(ps aux | grep -w mpd | grep -v grep | wc -l) -gt 0 ]; then
    mpc -p ${mpd_port-6600} current  -f "%title% ♬ %artist% ♪ %album%"
  fi
}

function do_add-todo() {
  cat >> "${HOME}/doc/work.org" <<EOF
* TODO $*
  SCHEDULED: <$(date --iso)>

EOF
}

function do_music-play-artists()
{
  mpc -p ${mpd_port-6600} clear

  for el in $@; do
    if [ $el = "music" ]; then
      continue
    fi

    mpc -p ${mpd_port-6600} add $el
  done

  mpc -p ${mpd_port-6600} shuffle
  mpc -p ${mpd_port-6600} play
}

function p4_check_that_everythings_ok()
{
  find . -type f -name "*" | grep -v target | grep -v easybook | grep -v iml$ | \
    while read f; do
      p4 filelog $f 1>/dev/null
    done
}

function do_set-wallpaper-current()
{
  local wallpaper=$HOME/pictures/wallpapers/current
  feh --bg-scale "${wallpaper}"
}

function do_set-wallpaper()
{
  wallpaper_names=$(ls ~/pictures/wallpapers/*.{jpg,png} 2>/dev/null)
  wallpaper_names=$(ls ~/pictures/wallpapers/atlantis/*.png 2>/dev/null)
  wallpaper_array=($wallpaper_names)
  number_of_wallpapers=${#wallpaper_array[*]}
  wallpaper=${wallpaper_array[$((RANDOM % number_of_wallpapers))]}
  sleep 4
  print "Setting a random wallpaper $(basename "${wallpaper}")"
  if [ -x /usr/bin/feh ]; then
    feh --bg-scale "${wallpaper}"
  else
    display -window root -backdrop "$wallpaper"
  fi

  killall xmessage &>/dev/null || true
}

function do_give-me-more-juice()
{
  local _apps="
/usr/lib/evolution/evolution-data-server/evolution-alarm-notify
/usr/lib/evolution/evolution-source-registry
evolution
slack
teams
  "

  local el=
  for el in ${_apps}; do
    killall ${el}
  done
}

function unknown_command()
{
  print "I don't know the command '${1}' :-("
  exit 1
}

read_user_conf_if_exists

fn=do_${1}

declare > /dev/null -f "$fn" || unknown_command "$1"
${fn} ${@:2}

# remove pid if it exists (created by
# alexanria/get_seconds_since_start
if [ -w "${pid_file}" ]; then
  rm $pid_file 2>/dev/null
fi

exit 0
