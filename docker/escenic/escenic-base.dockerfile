from debian:latest

maintainer torstein

arg ECE_APT_USER
arg ECE_APT_PASSWORD
arg ECE_APT_CODENAME
arg ECE_APT_ECE_SCRIPTS_CODENAME

ENV DEBIAN_FRONTEND=noninteractive

# add contrib and non-free suites
run sed -r -i "s#stretch main#stretch main contrib non-free#" \
    /etc/apt/sources.list

run apt-get update && \
    apt-get --yes install \
    curl \
    vim \
    gnupg

# Official Escenic & CUE releases
run echo "deb http://${ECE_APT_USER}:${ECE_APT_PASSWORD}@apt.escenic.com ${ECE_APT_CODENAME} contrib non-free" \
    >> /etc/apt/sources.list

# latest stable releases of the cloud tools, like ece and ece-install
run echo "deb http://apt.escenic.com ${ECE_APT_ECE_SCRIPTS_CODENAME} main non-free" \
    >> /etc/apt/sources.list

run curl --silent http://apt.escenic.com/repo.key | apt-key add -
run apt-get update && \
    apt-get install --yes escenic-content-engine-installer

run echo " \n\
credentials: \n\
  - site: maven.escenic.com \n\
    user: ${ECE_APT_USER} \n\
    password: ${ECE_APT_PASSWORD} \n\
  - site: yum.escenic.com \n\
    user: ${ECE_APT_USER} \n\
    password: ${ECE_APT_PASSWORD} \n\
  - site: apt.escenic.com \n\
    user: ${ECE_APT_USER} \n\
    password: ${ECE_APT_PASSWORD} \n\
\n\
environment: \n\
  java_oracle_licence_accepted: true \n\
\n\
" > /etc/ece-install.yaml
