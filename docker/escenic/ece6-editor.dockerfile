from skybert/escenic-base:latest

maintainer torstein

run echo " \n\
profiles: \n\
  editor: \n\
    install: yes \n\
\n\
  db: \n\
    host: db \n\
\n\
packages: \n\
  - name: escenic-content-engine-6.9 \n\
" >> /etc/ece-install.yaml


run bash -x ece-install -f /etc/ece-install.yaml

# Monkey patching
copy ece-scripts/usr/share/escenic/ece-scripts/ece.d/start.sh \
     /usr/share/escenic/ece-scripts/ece.d/start.sh
copy ece-scripts/usr/bin/ece /usr/bin/ece

run echo enable_remote_debugging=1 >> /etc/escenic/ece-engine1.conf

# Monkey patching ECE
copy engine-webservice-develop-SNAPSHOT.jar \
     /opt/tomcat-engine1/webapps/webservice/WEB-INF/lib/engine-webservice-6.9.2-2.jar

user escenic
cmd ["ece", "run"]

expose 5005
