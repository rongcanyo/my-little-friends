from skybert/escenic-base:latest

maintainer torstein

run echo " \n\
profiles: \n\
  cue: \n\
    install: yes \n\
    backend_ece: http://editor:8080 \n\
    cors_origins: \n\
      - cue \n\
\n\
packages: \n\
  - name: cue-web-3.0 \n\
" >> /etc/ece-install.yaml


run bash -x ece-install -f /etc/ece-install.yaml

# Monkey patching
copy ece-scripts/usr/share/escenic/ece-scripts/ece.d/start.sh \
     /usr/share/escenic/ece-scripts/ece.d/start.sh
copy ece-scripts/usr/bin/ece /usr/bin/ece

user escenic
cmd ["/sbin/ece7-cue.endpoint"]

expose 5005
