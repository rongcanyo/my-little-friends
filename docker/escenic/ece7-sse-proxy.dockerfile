from skybert/escenic-base:latest

maintainer torstein

run echo " \n\
profiles: \n\
  sse_proxy: \n\
    install: yes \n\
    exposed_host: 'sse-proxy' \n\
    backends: \n\
      - uri: http://editor:8080/webservice/escenic/changelog/sse \n\
        user: ding_admin \n\
        password: admin \n\
\n\
packages: \n\
  - name: escenic-sse-proxy-1.0 \n\
" >> /etc/ece-install.yaml


run apt-get update && \
    apt-get install -y openjdk-8-jdk-headless

run bash -x ece-install -f /etc/ece-install.yaml

# Monkey patching
copy ece-scripts/usr/share/escenic/ece-scripts/ece.d/start.sh \
     /usr/share/escenic/ece-scripts/ece.d/start.sh
copy ece-scripts/usr/bin/ece /usr/bin/ece

user escenic
copy $(basename "$0" .dockerfile).endpoint /sbin/
cmd ["sleep", "60"]
# cmd ["/sbin/ece7-sse-proxy.endpoint"]

expose 5005
