from skybert/escenic-base:latest

maintainer torstein

run echo " \n\
profiles: \n\
  db: \n\
    install: yes \n\
\n\
packages: \n\
  - name: escenic-content-engine-7.0 \n\
" >> /etc/ece-install.yaml

# Monkey patching, until
# https://github.com/escenic/ece-scripts/pull/55 is merged and
# released.
run sed -i 's#service mariadb start#service mysql start#' \
    /usr/share/escenic/ece-scripts/ece-install.d/database.sh

run bash -x ece-install -f /etc/ece-install.yaml

# Taken from: https://github.com/docker-library/mariadb/blob/4891ee2e3bd2dc6b07db634a39433ad579764a4b/10.3/Dockerfile
run find /etc/mysql/ -name '*.cnf' -print0 \
		| xargs -0 grep -lZE '^(bind-address|log)' \
		| xargs -rt -0 sed -Ei 's/^(bind-address|log)/#&/'; \
	echo '[mysqld]\nskip-host-cache\nskip-name-resolve' > \
        /etc/mysql/conf.d/docker.cnf

CMD ["mysqld"]
