# Build it with:
# docker build --tag skybert/michel  -f michel-orgmode.dockerfile .

from debian:latest
run export DEBIAN_FRONTEND=noninteractive && \
    echo 'deb http://ftp.no.debian.org/debian stretch-backports main' >> /etc/apt/sources.list && \
    apt-get update && \
    apt-get install -y \
            git \
            python-pip \
            python-oauth2client \
            python-dateutil \
            mercurial

workdir /usr/local/src
run hg clone https://bitbucket.org/edgimar/michel-orgmode && \
    pip install -e ./michel-orgmode

# run as me
run adduser --quiet --gecos "Torstein Krause Johansen,,,,"  torstein
workdir /home/torstein
user torstein
entrypoint ["michel-orgmode"]

