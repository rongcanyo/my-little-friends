tmux set -g status-style "bg=#222235,fg=#06c993"
tmux set -g status-right-length 150
tmux set -g status-right ""
tmux set -ga status-right "#h"
tmux set -ga status-right " 🐏 #(free -h | sed -n '2p' | awk '{print \$3 \"/\" \$2}')"
tmux set -ga status-right " 🔌 #(acpi | cut -d',' -f2 | tr -d ' ')"
tmux set -ga status-right " 🕘 %d %b %Y %H:%M "
